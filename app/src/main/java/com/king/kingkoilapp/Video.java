package com.king.kingkoilapp;

import android.app.Activity;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Video extends Activity implements SurfaceHolder.Callback{

    MediaPlayer mediaPlayer;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    boolean pausing = false;;

    VideoView mVideoView,mVideoView1,mVideoView2,mVideoView3;
    MediaController mediaControls;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video);



        getWindow().setFormat(PixelFormat.UNKNOWN);

        //Displays a video file.b
         mVideoView = (VideoView)findViewById(R.id.videoview);
        mVideoView1 = (VideoView)findViewById(R.id.videoview1);
//        mVideoView2 = (VideoView)findViewById(R.id.videoview3);
//        mVideoView3 = (VideoView)findViewById(R.id.videoview4);

        if (mediaControls == null) {
            // create an object of media controller class
            mediaControls = new MediaController(Video.this);
            mediaControls.setAnchorView(mVideoView);
            mediaControls.setAnchorView(mVideoView1);
//            mediaControls.setAnchorView(mVideoView2);
//            mediaControls.setAnchorView(mVideoView3);

        }

        mVideoView.setMediaController(mediaControls);
        mVideoView1.setMediaController(mediaControls);
        mVideoView2.setMediaController(mediaControls);
        mVideoView2.setMediaController(mediaControls);

        // set the uri for the video view
        mVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.kk_pdn_video));
        mVideoView1.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.kk_uae_history));
//        mVideoView2.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.kk_pdn_video));
//        mVideoView3.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.kk_pdn_video));
        // start a video
        mVideoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVideoView.start();
              //  mVideoView.
            }
        });
        mVideoView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVideoView1.start();
                //  mVideoView.
            }
        });

        // implement on completion listener on video view
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Toast.makeText(getApplicationContext(), "Thank You...!!!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
            }
        });
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(getApplicationContext(), "Oops An Error Occur While Playing Video...!!!", Toast.LENGTH_LONG).show(); // display a toast when an error is occured while playing an video
                return false;
            }
        });
        mVideoView1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Toast.makeText(getApplicationContext(), "Thank You...!!!", Toast.LENGTH_LONG).show(); // display a toast when an video is completed
            }
        });
        mVideoView1.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(getApplicationContext(), "Oops An Error Occur While Playing Video...!!!", Toast.LENGTH_LONG).show(); // display a toast when an error is occured while playing an video
                return false;
            }
        });
//        FileInputStream fileInputStream = null;
//        try {
//            fileInputStream = new FileInputStream(Environment.getExternalStorageDirectory()+"android.resource://"+getPackageName()+"/raw/myvideo");
//            mediaPlayer.setDataSource(fileInputStream.getFD());
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        String uriPath = "android.resource://com.android.AndroidVideoPlayer/"+R.raw.kk_pdn_video;
////        String uriPath = (Environment.getExternalStorageDirectory()+"android.resource://"+getPackageName()+"/raw/myvideo");
////       Uri uri = Uri.parse(uriPath);
//
//        mVideoView.requestFocus();
//        mVideoView.start();



//        mVideoView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                VideoView mVideoView = (VideoView)findViewById(R.id.videoview);
//
//               String uriPath ;
//               //= "android.resource://com.android.AndroidVideoPlayer/"+R.raw.k;
//
//              //  Uri uri = Uri.parse(uriPath);
//              //  mVideoView.setVideoURI(uri);
//                mVideoView.requestFocus();
//                mVideoView.start();
//            }
//        } );
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub

    }
}
