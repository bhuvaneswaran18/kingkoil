package com.king.kingkoilapp;

import java.util.List;

public class GetResponse {

    private List<ProductResponseModel> productList;


    public List<ProductResponseModel> getResponseModels() {
        return productList;
    }

    public void setResponseModels(List<ProductResponseModel> responseModels) {
        this.productList = responseModels;
    }
}
