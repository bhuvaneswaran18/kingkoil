package com.king.kingkoilapp;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ajts.androidmads.library.SQLiteToExcel;
import com.king.kingkoilapp.database.AppDatabase;
import com.king.kingkoilapp.database.FeedBackDao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class SubmitFeedback extends AppCompatActivity {
    Button submit;
    File myExternalFile;
    Button submit_scree;
    EditText ans, kingkoil_edt, description, edtname, edtp_o_box, edtemirate,
            edttel, edtmobile, edtemail;

    CheckBox store, advertisement, friend, website, neon, brandImage, quality, price, technology, yes, no;
    CheckBox excellent, good, average, excellent1, good1, average1, excellent2, good2, average2, excellent4, good4, average4;
    CheckBox linen, mattress, latex, headboard, comforter, none_of_the_above;
    CheckBox yes1, no1;
    TextView export_all;
    AppDatabase appDatabase;

    FeedBackDao feedBackDao;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.submit_feedback);


        //readRaw();
        store = findViewById(R.id.store);
        advertisement = findViewById(R.id.advertisement);
        friend = findViewById(R.id.friend);
        website = findViewById(R.id.website);
        neon = findViewById(R.id.neon);
        brandImage = findViewById(R.id.brand_image);
        quality = findViewById(R.id.quality);
        price = findViewById(R.id.price);
        technology = findViewById(R.id.technology);
        yes = findViewById(R.id.yes);
        no = findViewById(R.id.no);
        excellent = findViewById(R.id.excellent);
        good = findViewById(R.id.good);
        average = findViewById(R.id.average);
        excellent1 = findViewById(R.id.excellent1);
        good1 = findViewById(R.id.good1);
        average1 = findViewById(R.id.average1);
        excellent2 = findViewById(R.id.excellent2);
        good2 = findViewById(R.id.good2);
        average2 = findViewById(R.id.average2);
        excellent4 = findViewById(R.id.excellent4);
        good4 = findViewById(R.id.good4);
        average4 = findViewById(R.id.average4);
        linen = findViewById(R.id.linen);
        mattress = findViewById(R.id.mattress);
        latex = findViewById(R.id.latex);
        headboard = findViewById(R.id.headboard);
        comforter = findViewById(R.id.comforter);
        none_of_the_above = findViewById(R.id.none_of_the_above);

        yes1 = findViewById(R.id.yes1);
        no1 = findViewById(R.id.no1);
        submit_scree = findViewById(R.id.submit);


        submit = (Button) findViewById(R.id.submit);
        ans = (EditText) findViewById(R.id.edt_ans);
        kingkoil_edt = (EditText) findViewById(R.id.kingkoil_edt);
        description = (EditText) findViewById(R.id.description);
        edtname = (EditText) findViewById(R.id.edtname);
        edtp_o_box = (EditText) findViewById(R.id.edtp_o_box);
        edtemirate = (EditText) findViewById(R.id.edtemirate);
        edttel = (EditText) findViewById(R.id.edttel);
        edtmobile = (EditText) findViewById(R.id.edtmobile);
        edtemail = (EditText) findViewById(R.id.edtemail);
        export_all = findViewById(R.id.export_all);
        appDatabase = AppDatabase.getInstance(this);
        feedBackDao = appDatabase.feedBackDao();

        export_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dbname = "kingkoil";
                String dbpath = getDatabasePath(dbname).getPath();

                System.out.println("textDBNAME" + dbpath);

                String directory_path = Environment.getExternalStorageDirectory().getPath() + "/KingKoil Report/";
                File file = new File(directory_path);
                if (!file.exists()) {
                    file.mkdirs();
                }
                SQLiteToExcel sqliteToExcel = new SQLiteToExcel(SubmitFeedback.this, "kingkoil", directory_path);

                sqliteToExcel.exportSingleTable("feedbackModel", "kingkoil.xls", new SQLiteToExcel.ExportListener() {
                    @Override
                    public void onStart() {
                        System.out.println("testStart");

                    }

                    @Override
                    public void onCompleted(String filePath) {

                        Toast.makeText(SubmitFeedback.this, "Excel is saved in KingKoil Product", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Exception e) {
                        System.out.println("testOnError" + e);

                    }
                });

            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedBackDao.insert(getAnswerFeedBackModel());
                Toast.makeText(SubmitFeedback.this, "Feedback submitted successfully \n    Thank you for you feedback", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(SubmitFeedback.this, SubmitFeedback.class));
            }

        });


    }

    FeedBackModel getAnswerFeedBackModel() {


        FeedBackModel feedBackModel = new FeedBackModel();

        String indicate = (store.isChecked() ? store.getText().toString() : "") + "\n" +
                (advertisement.isChecked() ? advertisement.getText().toString() : "") + "\n" +
                (friend.isChecked() ? friend.getText().toString() : "") + "\n" + (website.isChecked() ? website.getText().toString() : "") + "\n" + (neon.isChecked() ? neon.getText().toString() : "");

        String indicateFactor = (brandImage.isChecked() ? brandImage.getText().toString() : "") + "\n" +
                (quality.isChecked() ? quality.getText().toString() : "") + "\n" + (price.isChecked() ? price.getText().toString() : "") + "\n" + (technology.isChecked() ? technology.getText().toString() : "") + "\n" + (technology.isChecked() ? technology.getText().toString() : "");

        String showRoomIncharge = (yes.isChecked() ? yes.getText().toString() : "") + "\n" + (no.isChecked() ? no.getText().toString() : "");
        String inchargeRate = "Friendly Behaviour    :" + (excellent.isChecked() ? excellent.getText().toString() : "") + "  " +
                (good.isChecked() ? good.getText().toString() : "") + "" + (average.isChecked() ? average.getText().toString() : "") + "\n" +
                "Product Knowledge    :" + (excellent1.isChecked() ? excellent1.getText().toString() : "") + "" + (good1.isChecked() ? good1.getText().toString() : "") + "" +
                (average1.isChecked() ? average1.getText().toString() : "")
                + "\n" + "Demonstration Skills    :" + (excellent2.isChecked() ? excellent2.getText().toString() : "") + "" +
                (good2.isChecked() ? good2.getText().toString() : "") + "" +
                (average2.isChecked() ? average2.getText().toString() : "");

        String indicateToBuy = (linen.isChecked() ? linen.getText().toString() : "") + "" +
                (mattress.isChecked() ? mattress.getText().toString() : "") + "" + (headboard.isChecked() ? headboard.getText().toString() : "") + "" + (comforter.isChecked() ? comforter.getText().toString() : "") + "" + (none_of_the_above.isChecked() ? none_of_the_above.getText().toString() : "");

        String ambianceDecor = (excellent4.isChecked() ? excellent4.getText().toString() : "") + "" +
                (good4.isChecked() ? good4.getText().toString() : "") + "" +
                (average4.isChecked() ? average4.getText().toString() : "");

        feedBackModel.setDid_our_showroom_incharge_help_you_reach_a_purchase_decision_by_providing_enough_information_technical_support_etc(showRoomIncharge);
        feedBackModel.setWhich_grade_of_mattress_did_you_purchase(ans.getText().toString());
        feedBackModel.setPlease_indicate_how_you_became_aware_of_King_Koil_product_you_have_just_purchased(indicate);
        feedBackModel.setWhich_other_King_Koil_brand_do_you_currently_own(kingkoil_edt.getText().toString());
        feedBackModel.setPlease_indicate_factors_that_most_influenced_your_decisions_to_purchase_this_King_Koil_product(indicateFactor);
        feedBackModel.setHow_do_you_rate_our_showroom_incharge_in_terms_of(inchargeRate);
        feedBackModel.setPlease_indicate_the_other_King_Koil_products_you_intend_to_buy(indicateToBuy);
        feedBackModel.setHow_do_you_rate_our_showroom_s_ambiance_décor(ambianceDecor);
        feedBackModel.setComments((yes1.isChecked() ? yes1.getText().toString() : "") + "" + (no1.isChecked() ? no1.getText().toString() : ""));
        feedBackModel.setName(edtname.getText().toString());
        feedBackModel.setPOBox(edtp_o_box.getText().toString());
        feedBackModel.setEmirate(edtemirate.getText().toString());
        feedBackModel.setTel(edttel.getText().toString());
        feedBackModel.setMobile(edtmobile.getText().toString());
        feedBackModel.setEmail(edtemail.getText().toString());

        return feedBackModel;
    }

}


