package com.king.kingkoilapp.repository;



import com.king.kingkoilapp.GetResponse;
import com.king.kingkoilapp.adapter.PromotionReponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface WebServices {

    @GET("rest_api.php")
    Call<GetResponse> getProductDetails(@QueryMap Map<String, Object> types);

    @GET("rest_api.php")
    Call<PromotionReponse> getPromotionList(@QueryMap Map<String, Object> types);

}
