package com.king.kingkoilapp.repository;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseRepository {


    public static Retrofit getClient() {

        Retrofit retrofit = null;
        try {


            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


            retrofit = new Retrofit.Builder()
                    .baseUrl("http://kingkoilme.com/apppanel/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        } catch (Exception e) {
            System.out.println("asdasdas" + e);
        }
        return retrofit;
    }
}
