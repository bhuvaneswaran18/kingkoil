package com.king.kingkoilapp.database;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.king.kingkoilapp.FeedBackModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;


@Dao
public interface FeedBackDao {

    @Insert(onConflict = REPLACE)
    void insert(FeedBackModel feedbackModel);


    @Query("SELECT * FROM feedbackModel")
    List<FeedBackModel> getModel();
}
