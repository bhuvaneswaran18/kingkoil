package com.king.kingkoilapp.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.king.kingkoilapp.FeedBackModel;
import com.king.kingkoilapp.ProductResponseModel;


@Database(entities = {ProductResponseModel.class, FeedBackModel.class}, exportSchema = false, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ProductDao productDao();

    public abstract FeedBackDao feedBackDao();

    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "kingkoil").fallbackToDestructiveMigration().allowMainThreadQueries().build();
        }
        return instance;
    }
}
