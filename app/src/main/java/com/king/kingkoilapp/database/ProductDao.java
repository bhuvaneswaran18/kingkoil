package com.king.kingkoilapp.database;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;


import com.king.kingkoilapp.ProductResponseModel;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;


@Dao
public interface ProductDao {

    @Insert(onConflict = REPLACE)
    void insert(List<ProductResponseModel> productResponseModel);


    @Query("SELECT * FROM productResponseModel where type= :typeCollection")
    List<ProductResponseModel> getProductDetails(int typeCollection);

    @Query("DELETE from productResponseModel where type=:typw")
    void deleteAll(int typw);
}
