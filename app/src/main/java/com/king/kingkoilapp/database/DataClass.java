package com.king.kingkoilapp.database;

import android.content.Context;
import android.util.Log;

import com.king.kingkoilapp.GetResponse;
import com.king.kingkoilapp.ListData;
import com.king.kingkoilapp.ProductResponseModel;
import com.king.kingkoilapp.PromotionInterface;
import com.king.kingkoilapp.Utils;
import com.king.kingkoilapp.adapter.PromotionReponse;
import com.king.kingkoilapp.repository.BaseRepository;
import com.king.kingkoilapp.repository.WebServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.king.kingkoilapp.ProductsScreen.lang;

public class DataClass {
    ListData mData;
    Context mContext;
    AppDatabase appDatabase;
    public int typeId = 0;
    ProductDao productDao;
    PromotionInterface promotionInterface;

    public DataClass(ListData data, Context context, int typeId) {
        mData = data;
        mContext = context;
        this.typeId = typeId;
        appDatabase = AppDatabase.getInstance(context);
        productDao = appDatabase.productDao();
    }

    public DataClass(PromotionInterface pr, Context context, int typeId) {
        this.promotionInterface = pr;
        mContext = context;
        this.typeId = typeId;
        appDatabase = AppDatabase.getInstance(context);
        productDao = appDatabase.productDao();
    }

    WebServices services = BaseRepository.getClient().create(WebServices.class);

    public void getData(Map<String, Object> request) {
        List<List<ProductResponseModel>> arrayListOfList = new ArrayList<>();
        if (Utils.isConnected(mContext) && Utils.isConnectedWifi(mContext)) {
            Call<GetResponse> apiCall = services.getProductDetails(request);
            apiCall.enqueue(new Callback<GetResponse>() {
                @Override
                public void onResponse(Call<GetResponse> call, Response<GetResponse> response) {
                    //need to re write this for best case

                    List<ProductResponseModel> responseModels = response.body().getResponseModels();

                    if (responseModels != null && responseModels.size() > 0) {
                        insert(responseModels);

                    }

                }

                @Override
                public void onFailure(Call<GetResponse> call, Throwable t) {
                    System.out.println("sdasasd" + t.toString());
                }
            });
        } else {
            getList();

        }
    }

    public void getPromotionData(Map<String, Object> request) {
        Call<PromotionReponse> apiCall = services.getPromotionList(request);
        apiCall.enqueue(new Callback<PromotionReponse>() {
            @Override
            public void onResponse(Call<PromotionReponse> call, Response<PromotionReponse> response) {
                if (!response.body().getPromotionList().isEmpty())
                    promotionInterface.data(response.body().getPromotionList());
            }

            @Override
            public void onFailure(Call<PromotionReponse> call, Throwable t) {
                System.out.println("sdasasd" + t.toString());
            }
        });


    }

    private HashMap<String, ArrayList<ProductResponseModel>> getProductList(List<ProductResponseModel> responseModels) {


        HashMap<String, ArrayList<ProductResponseModel>> hsmap = new HashMap<>();
        ArrayList<ProductResponseModel> tempModels;
        for (int j = 0; j < responseModels.size(); j++) {

            if (lang == 1) {
                if (hsmap.containsKey(responseModels.get(j).getCategory_name())) {
                    ArrayList<ProductResponseModel> dummyList = hsmap.get(responseModels.get(j).getCategory_name());
                    dummyList.add(responseModels.get(j));
                    hsmap.put(responseModels.get(j).getCategory_name(), dummyList);
                } else {
                    tempModels = new ArrayList<>();
                    tempModels.add(responseModels.get(j));
                    hsmap.put(responseModels.get(j).getCategory_name(), tempModels);

                }
            } else {
                if (hsmap.containsKey(responseModels.get(j).getCategory_name_ar())) {
                    ArrayList<ProductResponseModel> dummyList = hsmap.get(responseModels.get(j).getCategory_name_ar());
                    dummyList.add(responseModels.get(j));
                    hsmap.put(responseModels.get(j).getCategory_name_ar(), dummyList);
                } else {
                    tempModels = new ArrayList<>();
                    tempModels.add(responseModels.get(j));
                    hsmap.put(responseModels.get(j).getCategory_name_ar(), tempModels);

                }
            }
        }
        return hsmap;
    }

    private void insert(List<ProductResponseModel> list) {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setType(typeId);
        }
        productDao.deleteAll(typeId);
        productDao.insert(list);
        mData.data(getProductList(list));
    }

    private void getList() {
        List<ProductResponseModel> responseModels = new ArrayList<>();
        responseModels.addAll(productDao.getProductDetails(typeId));
        HashMap<String, ArrayList<ProductResponseModel>> tempData = getProductList(responseModels);
        System.out.println("sdasdasd" + tempData.size());
        mData.data(tempData);
    }

}
