package com.king.kingkoilapp;

public class RequestModel {


    public RequestModel(String type, String web) {
        this.type = type;
        this.web = web;
    }

    private String type;
    private String web;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
