package com.king.kingkoilapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

public class Home extends AppCompatActivity {
    GridView androidGridView;
    String[] homeString = {
            "Products By Type", "Accessories", "Promotions", "Scan Code", "Products By Collections", "Did you know",
            "Technology", "About Us",
    } ;
    int[] homeImageId = {
            R.drawable.kk_product_home, R.drawable.kk_product_home,R.drawable.kk_product_home, R.drawable.kk_product_home,
            R.drawable.kk_product_home, R.drawable.kk_product_home, R.drawable.kk_product_home,R.drawable.kk_product_home,


    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar in activity
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.home);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent =new Intent(Home.this,ProductsScreen.class);
        startActivity(intent);

    }
}
