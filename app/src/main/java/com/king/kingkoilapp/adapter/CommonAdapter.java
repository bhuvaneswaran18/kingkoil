package com.king.kingkoilapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.king.kingkoilapp.ProductResponseModel;
import com.king.kingkoilapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommonAdapter extends RecyclerView.Adapter<CommonAdapter.ViewHolder> {

    Context mContext;
    HashMap<String, ArrayList<ProductResponseModel>> mArrayListOfList;

    List<String> key;

    public CommonAdapter(Context context, HashMap<String, ArrayList<ProductResponseModel>> arrayListOfList, List<String> keys) {
        this.mContext = context;
        mArrayListOfList = arrayListOfList;
        key = keys;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_movie, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTitle.setText("" + key.get(position));
        CastAdapter castAdapter = new CastAdapter(mArrayListOfList.get(key.get(position)), mContext);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        holder.recyclerView.setLayoutManager(linearLayoutManager);

        holder.recyclerView.setAdapter(castAdapter);


    }

    public void updateList(HashMap<String, ArrayList<ProductResponseModel>> data, List<String> keys) {
        this.mArrayListOfList = data;
        this.key = keys;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return key.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        RecyclerView recyclerView;

        private ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.category_name);
            recyclerView = itemView.findViewById(R.id.recylerView);


        }
    }

}