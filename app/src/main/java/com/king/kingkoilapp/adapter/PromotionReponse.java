package com.king.kingkoilapp.adapter;

import java.util.List;

public class PromotionReponse {

    public List<PromotionList> getPromotionList() {
        return promotionList;
    }

    public void setPromotionList(List<PromotionList> promotionLists) {
        this.promotionList = promotionLists;
    }

    List<PromotionList> promotionList;


    public class PromotionList {
        String name;
        String name_ar;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName_ar() {
            return name_ar;
        }

        public void setName_ar(String name_ar) {
            this.name_ar = name_ar;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        String img;

    }
}
