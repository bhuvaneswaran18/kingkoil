package com.king.kingkoilapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.king.kingkoilapp.ImageViewActivity;
import com.king.kingkoilapp.ProductResponseModel;
import com.king.kingkoilapp.R;

import java.util.List;

import static com.king.kingkoilapp.ProductsScreen.lang;

public class CastAdapter extends RecyclerView.Adapter<CastAdapter.CastViewHolder> {


    List<ProductResponseModel> mProductList;
    Context mContext;

    public CastAdapter(@NonNull List<ProductResponseModel> productList, Context context) {
        mProductList = productList;
        mContext = context;
    }

    @NonNull
    @Override
    public CastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_cast, parent, false);
        return new CastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CastViewHolder holder, final int position) {
        if (lang == 1)
            holder.mName.setText(mProductList.get(position).getName());
        else
            holder.mName.setText(mProductList.get(position).getName_ar());

        RequestBuilder<Drawable> request = Glide.with(mContext)
                .load(mProductList.get(position).getImg()).diskCacheStrategy(DiskCacheStrategy.ALL);
        request.into(holder.actorImage);
        if (mProductList.get(position).getType() != 3) {
            holder.actorImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(mContext, ImageViewActivity.class);
                    intent.putExtra("url", mProductList.get(position).getDetail_img());
                    mContext.startActivity(intent);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }

    public class CastViewHolder extends RecyclerView.ViewHolder {
        private TextView mName;
        private ImageView actorImage;

        private CastViewHolder(View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.perfect_relief);
            actorImage = itemView.findViewById(R.id.imageView);
        }

    }
}