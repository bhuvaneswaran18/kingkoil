package com.king.kingkoilapp.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.king.kingkoilapp.R;

import java.util.List;

import static com.king.kingkoilapp.ProductsScreen.lang;

public class ImageAdapter extends PagerAdapter {
    Context context;


    LayoutInflater mLayoutInflater;
    List<PromotionReponse.PromotionList> promotionLists;

    public ImageAdapter(Context context, List<PromotionReponse.PromotionList> promotionLists) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.promotionLists = promotionLists;
    }

    @Override
    public int getCount() {
        return promotionLists.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        ImageView imageView = itemView.findViewById(R.id.imageViewPager);
        TextView textView = itemView.findViewById(R.id.text);
        if (lang == 1)
            textView.setText("" + promotionLists.get(position).getName());
        else
            textView.setText("" + promotionLists.get(position).getName_ar());
        Glide.with(context)
                .load(promotionLists.get(position).getImg())
                .into(imageView);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}