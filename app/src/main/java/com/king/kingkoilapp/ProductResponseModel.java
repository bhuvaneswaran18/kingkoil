package com.king.kingkoilapp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Keep;

@Keep
@Entity(tableName = "productResponseModel")
public class ProductResponseModel {

    @PrimaryKey(autoGenerate = true)
    public int uid;
    private String category_name;
    private String category_name_ar;
    private String name = "";
    private String name_ar;

    private String img;
    private String detail_img;
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] imgBlob;
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] detail_imgBlob;

    private int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_name_ar() {
        return category_name_ar;
    }

    public void setCategory_name_ar(String category_name_ar) {
        this.category_name_ar = category_name_ar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_ar() {
        return name_ar;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDetail_img() {
        return detail_img;
    }

    public void setDetail_img(String detail_img) {
        this.detail_img = detail_img;
    }

    public byte[] getImgBlob() {
        return imgBlob;
    }

    public void setImgBlob(byte[] imgBlob) {
        this.imgBlob = imgBlob;
    }

    public byte[] getDetail_imgBlob() {
        return detail_imgBlob;
    }

    public void setDetail_imgBlob(byte[] detail_imgBlob) {
        this.detail_imgBlob = detail_imgBlob;
    }
}
