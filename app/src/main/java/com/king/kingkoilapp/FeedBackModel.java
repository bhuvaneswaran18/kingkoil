package com.king.kingkoilapp;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Keep;

@Keep
@Entity(tableName = "feedbackModel", indices = @Index(value = {"Name"}, unique = true))
public class FeedBackModel {

    @PrimaryKey(autoGenerate = true)
    public int uid;
    public String Which_grade_of_mattress_did_you_purchase;
    public String Please_indicate_how_you_became_aware_of_King_Koil_product_you_have_just_purchased;
    public String Please_indicate_factors_that_most_influenced_your_decisions_to_purchase_this_King_Koil_product;
    public String Which_other_King_Koil_brand_do_you_currently_own;
    public String Did_our_showroom_incharge_help_you_reach_a_purchase_decision_by_providing_enough_information_technical_support_etc;
    public String How_do_you_rate_our_showroom_incharge_in_terms_of;
    public String How_do_you_rate_our_showroom_s_ambiance_décor;
    public String Please_indicate_the_other_King_Koil_products_you_intend_to_buy;
    public String Name;
    public String POBox;
    public String Emirate;
    public String Tel;
    public String Mobile;
    public String Email;
    public String Can_we_contact_you_for_any_of_your_comments;

    public String getWhich_grade_of_mattress_did_you_purchase() {
        return Which_grade_of_mattress_did_you_purchase;
    }

    public void setWhich_grade_of_mattress_did_you_purchase(String which_grade_of_mattress_did_you_purchase) {
        Which_grade_of_mattress_did_you_purchase = which_grade_of_mattress_did_you_purchase;
    }

    public String getPlease_indicate_how_you_became_aware_of_King_Koil_product_you_have_just_purchased() {
        return Please_indicate_how_you_became_aware_of_King_Koil_product_you_have_just_purchased;
    }

    public void setPlease_indicate_how_you_became_aware_of_King_Koil_product_you_have_just_purchased(String please_indicate_how_you_became_aware_of_King_Koil_product_you_have_just_purchased) {
        Please_indicate_how_you_became_aware_of_King_Koil_product_you_have_just_purchased = please_indicate_how_you_became_aware_of_King_Koil_product_you_have_just_purchased;
    }

    public String getPlease_indicate_factors_that_most_influenced_your_decisions_to_purchase_this_King_Koil_product() {
        return Please_indicate_factors_that_most_influenced_your_decisions_to_purchase_this_King_Koil_product;
    }

    public void setPlease_indicate_factors_that_most_influenced_your_decisions_to_purchase_this_King_Koil_product(String please_indicate_factors_that_most_influenced_your_decisions_to_purchase_this_King_Koil_product) {
        Please_indicate_factors_that_most_influenced_your_decisions_to_purchase_this_King_Koil_product = please_indicate_factors_that_most_influenced_your_decisions_to_purchase_this_King_Koil_product;
    }

    public String getWhich_other_King_Koil_brand_do_you_currently_own() {
        return Which_other_King_Koil_brand_do_you_currently_own;
    }

    public void setWhich_other_King_Koil_brand_do_you_currently_own(String which_other_King_Koil_brand_do_you_currently_own) {
        Which_other_King_Koil_brand_do_you_currently_own = which_other_King_Koil_brand_do_you_currently_own;
    }

    public String getDid_our_showroom_incharge_help_you_reach_a_purchase_decision_by_providing_enough_information_technical_support_etc() {
        return Did_our_showroom_incharge_help_you_reach_a_purchase_decision_by_providing_enough_information_technical_support_etc;
    }

    public void setDid_our_showroom_incharge_help_you_reach_a_purchase_decision_by_providing_enough_information_technical_support_etc(String did_our_showroom_incharge_help_you_reach_a_purchase_decision_by_providing_enough_information_technical_support_etc) {
        Did_our_showroom_incharge_help_you_reach_a_purchase_decision_by_providing_enough_information_technical_support_etc = did_our_showroom_incharge_help_you_reach_a_purchase_decision_by_providing_enough_information_technical_support_etc;
    }

    public String getHow_do_you_rate_our_showroom_incharge_in_terms_of() {
        return How_do_you_rate_our_showroom_incharge_in_terms_of;
    }

    public void setHow_do_you_rate_our_showroom_incharge_in_terms_of(String how_do_you_rate_our_showroom_incharge_in_terms_of) {
        How_do_you_rate_our_showroom_incharge_in_terms_of = how_do_you_rate_our_showroom_incharge_in_terms_of;
    }

    public String getHow_do_you_rate_our_showroom_s_ambiance_décor() {
        return How_do_you_rate_our_showroom_s_ambiance_décor;
    }

    public void setHow_do_you_rate_our_showroom_s_ambiance_décor(String how_do_you_rate_our_showroom_s_ambiance_décor) {
        How_do_you_rate_our_showroom_s_ambiance_décor = how_do_you_rate_our_showroom_s_ambiance_décor;
    }

    public String getPlease_indicate_the_other_King_Koil_products_you_intend_to_buy() {
        return Please_indicate_the_other_King_Koil_products_you_intend_to_buy;
    }

    public void setPlease_indicate_the_other_King_Koil_products_you_intend_to_buy(String please_indicate_the_other_King_Koil_products_you_intend_to_buy) {
        Please_indicate_the_other_King_Koil_products_you_intend_to_buy = please_indicate_the_other_King_Koil_products_you_intend_to_buy;
    }


    public String getComments() {
        return Can_we_contact_you_for_any_of_your_comments;
    }

    public void setComments(String comments) {
        Can_we_contact_you_for_any_of_your_comments = comments;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPOBox() {
        return POBox;
    }

    public void setPOBox(String POBox) {
        this.POBox = POBox;
    }

    public String getEmirate() {
        return Emirate;
    }

    public void setEmirate(String emirate) {
        Emirate = emirate;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String tel) {
        Tel = tel;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }


}
