package com.king.kingkoilapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


import com.king.kingkoilapp.adapter.ImageAdapter;
import com.king.kingkoilapp.adapter.PromotionReponse;
import com.king.kingkoilapp.adapter.VideoViewPagerAdapter;
import com.king.kingkoilapp.database.DataClass;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PromotionVideoActivity extends AppCompatActivity implements PromotionInterface {

    ViewPager viewPager;
    int fromIntent = 0;
    Button back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_image_activity);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        fromIntent = getIntent().getIntExtra("fromIntent", 0);
        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (fromIntent == 1) {
            VideoViewPagerAdapter viewPagerAdapter = new VideoViewPagerAdapter(PromotionVideoActivity.this);
            viewPager.setAdapter(viewPagerAdapter);
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("type", "4");
            map.put("web", "2");
            DataClass dataClass = new DataClass(this, this, 2);
            dataClass.getPromotionData(map);

        }
    }

    @Override
    public void data(List<PromotionReponse.PromotionList> data) {
        if (!data.isEmpty()) {
            ImageAdapter imageAdapter = new ImageAdapter(PromotionVideoActivity.this, data);
            viewPager.setAdapter(imageAdapter);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
