package com.king.kingkoilapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.Objects;


public class ProductsScreen extends Activity {
    Button submit, close;
    public final static int QRcodeWidth = 350;
    ImageView prdt_by_type, prdt_by_collectn, accessories, promotions, did_u_know, technology,
            about_us, scan;
    Intent intent;

    public static int lang = 0;
    private static int requestCode = 100;
    public static final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar in activity
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.product);
        submit = findViewById(R.id.submit);
        close = findViewById(R.id.close);
        prdt_by_type = findViewById(R.id.prdt_by_type);
        prdt_by_collectn = findViewById(R.id.prdt_by_collectn);
        accessories = findViewById(R.id.accessories);
        promotions = findViewById(R.id.promotions);
        did_u_know = findViewById(R.id.did_u_know);
        technology = findViewById(R.id.technology);
        about_us = findViewById(R.id.about_us);
        scan = findViewById(R.id.video);
        requestStoragePermission(requestCode);

        int language = getIntent().getIntExtra("lang", 0);
        if (language == 2) {
            lang = 2;
            prdt_by_type.setImageDrawable(getResources().getDrawable(R.drawable.pdts_by_type_main_ar));
            prdt_by_collectn.setImageDrawable(getResources().getDrawable(R.drawable.pdts_by_coltn_main_ar));
            accessories.setImageDrawable(getResources().getDrawable(R.drawable.accessories_ar));
            promotions.setImageDrawable(getResources().getDrawable(R.drawable.promotions_main_ar));
            did_u_know.setImageDrawable(getResources().getDrawable(R.drawable.didukw_main_ar));
            about_us.setImageDrawable(getResources().getDrawable(R.drawable.about_us_main_ar));
            technology.setImageDrawable(getResources().getDrawable(R.drawable.technology_main_ar));
            scan.setImageDrawable(getResources().getDrawable(R.drawable.video_p));
        } else
            lang = 1;

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ProductsScreen.this, MainActivity.class);
                startActivity(intent);
            }
        });

        prdt_by_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ProductsScreen.this, ProductByType.class);
                startActivity(intent);
            }
        });
        prdt_by_collectn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ProductsScreen.this, Products_By_Collection.class);
                startActivity(intent);
            }
        });
        promotions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ProductsScreen.this, PromotionVideoActivity.class);
                startActivity(intent);
            }
        });
        did_u_know.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ProductsScreen.this, DidUKnow.class);
                startActivity(intent);
            }
        });
        technology.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ProductsScreen.this, Technology.class);
                startActivity(intent);
            }
        });
        about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ProductsScreen.this, AboutUs.class);
                startActivity(intent);
            }
        });
        accessories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ProductsScreen.this, Accessories.class);
                startActivity(intent);
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ProductsScreen.this, SubmitFeedback.class);
                startActivity(intent);
            }
        });
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intent = new Intent(ProductsScreen.this, PromotionVideoActivity.class);
                intent.putExtra("fromIntent", 1);
                startActivity(intent);

            }
        });
    }


    private void requestStoragePermission(int requestCOde) {
        if (isExplanationNeeded(Objects.requireNonNull(this), WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, requestCOde);
        } else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, requestCOde);
        }
    }

    public static boolean isExplanationNeeded(@NonNull Activity context, @NonNull String permissionName) {
        return ActivityCompat.shouldShowRequestPermissionRationale(context, permissionName);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intent = new Intent(ProductsScreen.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
