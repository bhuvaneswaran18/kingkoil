package com.king.kingkoilapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface ListData {

    void data(HashMap<String, ArrayList<ProductResponseModel>> data);
}
