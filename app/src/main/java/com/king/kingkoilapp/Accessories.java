package com.king.kingkoilapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.king.kingkoilapp.Model.Accessorymodel;
import com.king.kingkoilapp.adapter.CommonAdapter;
import com.king.kingkoilapp.database.DataClass;
import com.king.kingkoilapp.repository.WebServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Accessories extends Activity implements ListData {
    Button back;
    TextView crest_pillow_description, bliss_description;
    // private RecyclerView recyclerView;
    // private ArrayList<Accessorymodel> imageModelArrayList;
    // private AccessoryAdapter adapter;
    WebServices services;
    RecyclerView recyclerView;
    ArrayList<String> keys = new ArrayList<>();
    HashMap<String, ArrayList<ProductResponseModel>> mArrayListOfList = new HashMap<>();
    CommonAdapter adapter;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar in activity
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.pdt_by_collectn);
        progressBar = findViewById(R.id.progress_bar);

        recyclerView = (RecyclerView) findViewById(R.id.recylerView);
        adapter = new CommonAdapter(Accessories.this, mArrayListOfList, keys);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);


        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Accessories.this, ProductsScreen.class);
                startActivity(intent);
            }
        });


        Map<String, Object> map = new HashMap<>();
        map.put("type", "3");
        map.put("web", "1");

        DataClass dataClass = new DataClass(this, this, 3);
        dataClass.getData(map);


        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void data(HashMap<String, ArrayList<ProductResponseModel>> data) {
        if (!data.isEmpty()) {
            List<String> keys = new ArrayList<>(data.keySet());
            adapter.updateList(data, keys);
            progressBar.setVisibility(View.GONE);
        }

    }
}
