package com.king.kingkoilapp;


import com.king.kingkoilapp.adapter.PromotionReponse;

import java.util.List;

public interface PromotionInterface {

    void data(List<PromotionReponse.PromotionList> data);
}
