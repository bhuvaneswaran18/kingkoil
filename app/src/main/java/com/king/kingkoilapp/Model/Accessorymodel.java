package com.king.kingkoilapp.Model;

public class Accessorymodel {

    private String name;
    private String description;
    private int image_drawable;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage_drawable() {
        return image_drawable;
    }

    public void setImage_drawable(int image_drawable) {
        this.image_drawable = image_drawable;
    }
    public String getdescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

