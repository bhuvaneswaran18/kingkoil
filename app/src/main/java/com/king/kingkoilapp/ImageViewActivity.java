package com.king.kingkoilapp;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class ImageViewActivity extends AppCompatActivity {
    ImageView full__image_view;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        full__image_view = (ImageView) findViewById(R.id.full__image_view);
        url = getIntent().getStringExtra("url");
        Log.i("","Detailed Image");

        try {


            RequestBuilder<Drawable> request = Glide.with(this)
                    .load(url).diskCacheStrategy(DiskCacheStrategy.ALL);
            request.listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    Log.i("", "onLoadFailed : ", e);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                    Log.i("", "onResourceReday");
                    return false;
                }
            });
            request.into(full__image_view);



            /*RequestCreator requestCreator = Picasso.get().load(url).error(R.drawable.premiumhybrid);
            requestCreator.fetch(new Callback() {
                @Override
                public void onSuccess() {

                    System.out.println("success");
                }

                @Override
                public void onError(Exception e) {
                    System.out.println("asdasda" + e);

                }
            });
            requestCreator.into(imageView);*/

        } catch (Exception e) {
        }
    }
}
